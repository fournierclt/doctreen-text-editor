Based on the [@ckeditor5/ckeditor5-build-classic](https://github.com/ckeditor/ckeditor5/tree/master/packages/ckeditor5-build-classic)

This is intended to be used inside our application as permited by the rightful owners [license](https://ckeditor.com/legal/ckeditor-oss-license/).
Use their original repository.

This repo will go private as soon as our structure allows it.